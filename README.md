# cal8tor • *cal*endar P*8* extrac*tor*

Extracteur d'emploi du temps pour la licence d'informatique de Paris 8

[![dependency status](https://deps.rs/repo/gitea/git.mylloon.fr/Paris8/cal8tor/status.svg)](https://deps.rs/repo/gitea/git.mylloon.fr/Paris8/cal8tor)

## Compilation et installation

Vous aurez besoin de Rust pour compiler le programme.

<details><summary>Vous avez aussi besoin d'<code>OpenSSL</code>.</summary>

- Ubuntu: `sudo apt install libssl-dev`
- Fedora: `dnf install openssl-devel`
</details>

1. Clone le dépôt et s'y rendre

```bash
$ git clone https://git.mylloon.fr/Paris8/cal8tor.git && cd cal8tor
```

2. Compiler et lancer l'application

```bash
$ cargo r --release
```

## Lancer

Pour afficher la page d'aide

```
$ cargo r --release -- --help
```

## Voir le calendrier dans le terminal

Pour les L2-X par exemple, lancez :

```bash
$ cargo r --release -- l2-X
```

> Le rendu peut parfois être difficile à lire, n'hésites pas à utiliser l'option
> `-c` (ou `--cl`) pour ajuster la longueur des cellules du planning.

## Exporter le calendrier au format `.ics`

Pour les L1-A par exemple, lancez :

```bash
$ cargo r --release -- L1A --export calendar.ics
```

> Le fichier comprend le fuseau horaire pour `Europe/Paris` et est
> conforme à [cet outil de validation](https://icalendar.org/validator.html).
