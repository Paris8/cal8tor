\documentclass{article}

\usepackage[T1]{fontenc} % encodage
\renewcommand{\familydefault}{\sfdefault} % police en sans-serif

\usepackage[french]{babel} % langue
\frenchsetup{SmallCapsFigTabCaptions=false}

\usepackage[hidelinks]{hyperref} % liens cliquable dans la table des matières

\usepackage{graphicx} % images
\usepackage{caption}

\usepackage[a4paper, left=20mm, top=20mm]{geometry} % dimensions de la page

\usepackage{minted} % intégration code
\usemintedstyle{emacs}

% Alias
\newcommand{\dd}{-\---} % double dash
\newcommand{\q}[1]{\og #1 \fg} % quotes

\title{Projet - Scraper}
\author{\href{mailto:anri.kennel@etud.univ-paris8.fr}{
    Anri Kennel}\thanks{Numéro d'étudiant : 20010664}\, (L3-Y)
    \\Ingénierie des langues $\cdot$ Université Paris 8}
\date{Année universitaire 2022-2023}

\begin{document}
\maketitle
\tableofcontents
\clearpage

\section{Projet}
Dans ce projet j'ai créé un scraper qui récupère les emplois du temps
des étudiants depuis le site de la licence informatique de Paris 8.
J'ai ensuite étendu le projet en traitant les données récupérées afin de
proposer deux résultats :
\begin{itemize}
    \item un fichier de sortie au format \texttt{.ics}
    \item une sortie dans le terminal sous forme de tableau
\end{itemize}

\vspace{1em}
Ce projet s'inscrit à la fois dans la création de corpus car il récupère depuis
une site internet des données et à la fois dans l'analyse textuelle car il
analyse les données récoltés, les traite, et produit une fichier utilisable.
Le programme propose une interface simple via des arguments passés en ligne de
commande, pour cela j'ai utilisé le module \texttt{clap}.

\vspace{1em}
J'ai choisi de faire ce projet parce que :
\begin{itemize}
    \item scraping de site web
    \item utile : permet de récupérer un emploi du temps exportable vers
          une application tierce que j'ai utilisé toute l'année
    \item utilisation de formule regex comme vue en cours
\end{itemize}

\section{Implémentation}
\subsection{Scraper}
Le programme est écrit en Rust.
Pour faire les requêtes sur les pages web, j'ai utilisé les modules
\texttt{reqwest} et \texttt{tokio}.
Pour la partie scraping, j'ai utilisé le module
\texttt{scraper}. Ensuite pour le traitement des données, j'ai ajouté
l'utilisation de \texttt{regex}. J'ai aussi utilisé le module \texttt{chrono}
qui m'as permis de traiter les dates.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{imgs/from_example.png}
    \caption{Affichage de l'emploi du temps des L3-Y depuis le site web}
    \label{fig:l3y-website}
\end{figure}

\clearpage
\subsection{Données récupérées}
Deux pages sont scrapées par le programme :
\begin{enumerate}
    \item \label{1} La page d'emploi du temps principal : \url{
              https://informatique.up8.edu/licence-iv/edt/}
          \begin{itemize}
              \item Cette page contient les informations nécessaires pour
                    connaître la position temporelle des cours (exemple :
                    quelle année ? quel mois commence les cours ? combien de
                    temps les cours durent ?)
          \end{itemize}
    \item \label{2} La page qui contient l'emploi du temps : \url{
              https://informatique.up8.edu/licence-iv/edt/CLASSE.html} avec
          \texttt{CLASSE} demandée par l'utilisateur
          \begin{itemize}
              \item Cette page contient l'emploi du temps en tant quel tel, avec
                    les noms des matières, des professeurs et les numéros de
                    salles.
          \end{itemize}
\end{enumerate}

\begin{itemize}
    \item Le point \no\ref{2} est traité dans un premier temps dans le fichier
          \texttt{src/timetable.rs}
          Les cours ainsi récupérés sont récupérés dans une structure
          \texttt{Course} définie dans \texttt{src/timetable/models.rs}
    \item Le point \no\ref{1} est traité dans un second temps dans le fichier
          \texttt{info.rs}. Elle n'est faite que lorsque l'on veut générer un
          calendrier au format \texttt{.ics}, sinon le repère temporelle
          n'est pas nécessaire.
\end{itemize}

\section{Calendrier}
Après scraping, on traite l'information.

\subsection{Fichier}
Lors de l'exportation du calendrier dans un fichier, au format \texttt{.ics},
on utilise le module \texttt{ics} pour la structure du fichier, et \texttt{uuid}
pour générer des ID unique pour chaque évènement.

Le fichier en sortie est conforme à la \href{
    https://icalendar.org/RFC-Specifications/iCalendar-RFC-5545/
}{norme RFC 5545} et est donc compatible avec les programmes de calendriers,
pour en être sûr j'ai utilisé le validateur mis à disposition sur
\url{https://icalendar.org/validator.html}.

\begin{figure}[h]
    \begin{minted}[autogobble,fontsize=\footnotesize]{text}
        BEGIN:VEVENT
        UID:b41164e1-fe45-4f82-83cf-c69f89f25614
        DTSTAMP:20230419T090534Z
        CLASS:PUBLIC
        TRANSP:OPAQUE
        DESCRIPTION:Anna Pappa
        DTSTART;TZID=Europe/Paris:20230214T150000
        DTEND;TZID=Europe/Paris:20230214T180000
        LOCATION:B104
        SUMMARY;LANGUAGE=fr:Ingénierie des langues
        END:VEVENT
    \end{minted}
    \caption{Extrait de l'emploi du temps des L3-Y sous forme de fichier
        (\footnotesize{\texttt{cargo r \dd{}release \dd{} l3-y -e file}})}
    \label{txt:l3y-file}
\end{figure}

\clearpage
\subsection{Terminal}
Par défaut, on affiche le calendrier sous forme de tableau dans la sortie
standard du terminal. Pour avoir un affichage différent que le site web,
j'ai décidé de mettre les jours en colonnes et les heures en lignes.

\begin{figure}[h]
    \hspace{-5em}
    \includegraphics[width=1.29\textwidth]{imgs/output_example.png}
    \caption{Affichage de l'emploi du temps des L3-Y sur la sortie standard}
    \label{fig:l3y-cli}
\end{figure}

\section{Exemple d'utilisation}
Voici explication de comment utiliser le programme :
\begin{description}
    \item[\texttt{<CLASS>}] spécifie quelle classe on veut récupérer l'emploi
        du temps, elle peut être écrite dans plusieurs formats, tant
        qu'il n'y a pas d'espaces, exemples :
        \q{L3A} \q{L3-a} \q{l3$\cdot$A} etc.
        \begin{itemize}
            \item[$\rightarrow$] \texttt{cargo r \dd{}release \dd{} L3Y}
        \end{itemize}
    \item[\texttt{-e, \dd{}export <file>}] permet d'exporter dans un fichier
        l'emploi du temps (cf. \autoref{txt:l3y-file})
        \begin{itemize}
            \item[$\rightarrow$] \texttt{cargo r \dd{}release \dd{} L3Y -e file}
        \end{itemize}
    \item[\texttt{-h, \dd{}help}] affiche un message d'aide similaire à ce
        bloc descriptif
        \begin{itemize}
            \item[$\rightarrow$] \texttt{cargo r \dd{}release \dd{} -h}
        \end{itemize}
\end{description}

\vspace{1em}
Par défaut, le programme affiche l'emploi du temps dans la sortie standard
(cf. \autoref{fig:l3y-cli}) et ne l'exporte pas dans un fichier.
\begin{itemize}
    \item[$\rightarrow$] \texttt{cargo r \dd{}release}
\end{itemize}

\appendix
\section*{Appendix}
\subsection*{Dépendances}
Vous avez besoin de Rust pour la compilation et de OpenSSL pour les requêtes
internet.

\subsubsection*{Rust}
\begin{figure}[h]
    \begin{minted}[autogobble,fontsize=\footnotesize]{text}
        curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    \end{minted}
\end{figure}

\end{document}
